#include <iostream>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/crop_box.h>
#include <chrono>
#include <thread>
#include <unordered_set>
#include <pcl/search/kdtree.h>
#include <pcl/segmentation/extract_clusters.h>

class Filter
{
private:
    /* data */
public:
    Filter(/* args */);
    ~Filter();

    // 01. FILTER
    static pcl::PointCloud<pcl::PointXYZ>::Ptr filterCloud(
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
        float                                 filterRes,
        Eigen::Vector4f                       minPoint,
        Eigen::Vector4f                       maxPoint);

    static std::pair<pcl::PointCloud<pcl::PointXYZ>::Ptr, pcl::PointCloud<pcl::PointXYZ>::Ptr> RansacPlane(
            pcl::PointCloud<pcl::PointXYZ>::Ptr   cloud,
            int                                   maxIterations,
            float                                 distanceTol);

    static std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> Clustering(pcl::PointCloud<pcl::PointXYZ>::Ptr   cloud){
        pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
        tree->setInputCloud (cloud);

        std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> clusters;

        std::vector<pcl::PointIndices> cluster_indices;
        pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec;
        ec.setClusterTolerance (0.4); // 2cm
        ec.setMinClusterSize (50);
        ec.setMaxClusterSize (25000);
        ec.setSearchMethod (tree);
        ec.setInputCloud (cloud);
        ec.extract (cluster_indices);

        int j = 0;
        for (pcl::PointIndices getIndices : cluster_indices) {
            pcl::PointCloud<pcl::PointXYZ>::Ptr cloudCluster (new pcl::PointCloud<pcl::PointXYZ>);

            for (int index : getIndices.indices) {
            cloudCluster->points.push_back(cloud->points[index]);
            }

            cloudCluster->width = cloudCluster->points.size();
            cloudCluster->height = 1;
            cloudCluster->is_dense = true;

            clusters.push_back(cloudCluster);
        }
        return clusters;
    }
};
