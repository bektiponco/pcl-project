#include "Filter.h"

Filter::Filter(/* args */)
{
}

Filter::~Filter()
{
}

// FILTER
pcl::PointCloud<pcl::PointXYZ>::Ptr Filter::filterCloud(
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
        float                                 filterRes,
        Eigen::Vector4f                       minPoint,
        Eigen::Vector4f                       maxPoint)
    {
        // Time segmentation process
        // auto startTime = std::chrono::steady_clock::now();

        // Create the filtering object: downsample the dataset using a leaf size of 0.2m
        pcl::VoxelGrid<pcl::PointXYZ> vg;
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudFiltered (new pcl::PointCloud<pcl::PointXYZ>);
        vg.setInputCloud(cloud);
        vg.setLeafSize(filterRes, filterRes, filterRes);
        vg.filter(*cloudFiltered);

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudRegion (new pcl::PointCloud<pcl::PointXYZ>);

        // Filter Region of interest
        pcl::CropBox<pcl::PointXYZ> region(true);
        region.setMin(minPoint);
        region.setMax(maxPoint);
        region.setInputCloud(cloudFiltered);
        region.filter(*cloudRegion);

        std::vector<int> indices;

        // Filter points on the roof top
        pcl::CropBox<pcl::PointXYZ> roof(true);
        roof.setMin(Eigen::Vector4f(-1.5, -1.7, -1, 1));
        roof.setMax(Eigen::Vector4f(2.6, 1.7, -0.4, 1));
        roof.setInputCloud(cloudRegion);
        roof.filter(indices);

        pcl::PointIndices::Ptr inliers {new pcl::PointIndices};
        for (int point : indices) {
            inliers->indices.push_back(point);
        }

        // Remove roof points
        pcl::ExtractIndices<pcl::PointXYZ> extract;
        extract.setInputCloud(cloudRegion);
        extract.setIndices(inliers);
        extract.setNegative(true);
        extract.filter(*cloudRegion);

        // auto endTime = std::chrono::steady_clock::now();
        // auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
        // std::cout << "filtering took " << elapsedTime.count() << " milliseconds" << std::endl;

        return cloudRegion;
    }

std::pair<pcl::PointCloud<pcl::PointXYZ>::Ptr, pcl::PointCloud<pcl::PointXYZ>::Ptr> Filter::RansacPlane(
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud,
    int                                   maxIterations,
    float                                 distanceTol)
{
    // Time segmentation process
    // auto startTime = std::chrono::steady_clock::now();

    std::unordered_set<int> inliersResult;
    srand(time(nullptr));

    // For max iterations
    while (maxIterations--) {
        std::unordered_set<int> inliers;

        // Randomly pick 2 points
        while (inliers.size() < 3) {
        inliers.insert(rand() % (cloud->points.size()));
        }

        float x1, y1, z1;
        float x2, y2, z2;
        float x3, y3, z3;

        auto itr = inliers.begin();

        x1 = cloud->points[*itr].x;
        y1 = cloud->points[*itr].y;
        z1 = cloud->points[*itr].z;
        itr++;
        x2 = cloud->points[*itr].x;
        y2 = cloud->points[*itr].y;
        z2 = cloud->points[*itr].z;
        itr++;
        x3 = cloud->points[*itr].x;
        y3 = cloud->points[*itr].y;
        z3 = cloud->points[*itr].z;

        float a = (y2 - y1) * (z3 - z1) - (z2 - z1) * (y3 - y1);
        float b = (z2 - z1) * (x3 - x1) - (x2 - x1) * (z3 - z1);
        float c = (x2 - x1) * (y3 - y1) - (y2 - y1) * (x3 - x1);
        float d = -(a * x1 + b * y1 + c * z1);

        for (int index = 0; index < cloud->points.size(); ++index) {
        if (inliers.count(index) > 0) {
            continue;
        }

        pcl::PointXYZ point = cloud->points[index];
        float x4 = point.x;
        float y4 = point.y;
        float z4 = point.z;

        float dist = fabs(a * x4 + b * y4 + c * z4 + d) / sqrt(a * a + b * b + c * c);

        // If distance is smaller than threshold count it as inlier
        if (dist <= distanceTol) {
            inliers.insert(index);
        }
        }

        if (inliers.size() > inliersResult.size()) {
        inliersResult = inliers;
        }
    }

    // auto endTime = std::chrono::steady_clock::now();
    // auto elapsedTime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime);
    // std::cout << "Plane RANSAC took " << elapsedTime.count() << " milliseconds" << std::endl;

    typename pcl::PointCloud<pcl::PointXYZ>::Ptr cloudInliers(new pcl::PointCloud<pcl::PointXYZ>());
    typename pcl::PointCloud<pcl::PointXYZ>::Ptr cloudOutliers(new pcl::PointCloud<pcl::PointXYZ>());

    for (int index = 0; index < cloud->points.size(); ++index) {
        pcl::PointXYZ point = cloud->points[index];

        if (inliersResult.count(index)) {
        cloudInliers->points.push_back(point);
        }
        else {
        cloudOutliers->points.push_back(point);
        }
    }

    return std::pair<typename pcl::PointCloud<pcl::PointXYZ>::Ptr,
                    typename pcl::PointCloud<pcl::PointXYZ>::Ptr>(cloudOutliers, cloudInliers);
}