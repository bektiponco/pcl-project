#include "EnginePCL.h"
#include "tools/Filter.h"

std::vector<boost::filesystem::path> EnginePCL::streamPcd(std::string dataPath)
{
    std::vector<boost::filesystem::path> paths(
    boost::filesystem::directory_iterator{dataPath},
    boost::filesystem::directory_iterator{});

    sort(paths.begin(), paths.end());
    return paths;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr EnginePCL::loadPCD(std::string file){
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    if (pcl::io::loadPCDFile<pcl::PointXYZ> (file, *cloud) == -1) //* load the file
    {
        PCL_ERROR ("Couldn't read file pcd \n");
    }
    // std::cerr << "Loaded " << cloud->points.size () << " data points from "+file << std::endl;
    return cloud;
}

void EnginePCL::renderPointCloud(
        pcl::visualization::PCLVisualizer::Ptr&    viewer,
        const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud,
        std::string                                name,
        EnginePCL::Color                           color)
    {
        viewer->addPointCloud<pcl::PointXYZ>(cloud, name);
        viewer->setPointCloudRenderingProperties(
            pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, name);
        viewer->setPointCloudRenderingProperties(
            pcl::visualization::PCL_VISUALIZER_COLOR,
            color.r, color.g, color.b,
            name);
    }

void EnginePCL::start(std::vector<boost::filesystem::path>  stream){

    std::string name = "Testing";
    pcl::visualization::PCLVisualizer::Ptr viewer (new pcl::visualization::PCLVisualizer ("PCD Viewer"));
    auto streamIterator = stream.begin();
    pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud;

    int oldMillis = 0;
    while (!viewer->wasStopped ())
    {
        auto now = std::chrono::system_clock::now();
        std::chrono::milliseconds unix_timestamp = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        int currentMillis = unix_timestamp.count();

        if(currentMillis-oldMillis > 2000){

            // Proses cloud
            inputCloud = loadPCD((*streamIterator).string());

            // Step 1: VOXEL FILTER
            inputCloud = Filter::filterCloud(inputCloud, 0.3, Eigen::Vector4f (-20, -6, -3, 1), Eigen::Vector4f ( 30, 7, 2, 1));
            
            // Step 2: Segment the filtered cloud into two parts, road and obstacles
            std::pair<pcl::PointCloud<pcl::PointXYZ>::Ptr, 
            pcl::PointCloud<pcl::PointXYZ>::Ptr> obsRoad = Filter::RansacPlane(inputCloud, 100, 0.2);

            // Step 3: Clustering
            std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> clusters = Filter::Clustering(obsRoad.first);

            // Clear viewer
            viewer->removeAllPointClouds();
            viewer->removeAllShapes();

            // render to viewer
            // renderPointCloud(viewer, obsRoad.first, "obstacles", Color(1, 0, 0));
            int indexCluster = 0;
            viewer->setWindowName((*streamIterator).string()+" "+std::to_string(clusters.size()));
            for(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_cluster : clusters){
                renderPointCloud(viewer, cloud_cluster, "obstacles_"+std::to_string(indexCluster), Color::random());
                indexCluster++;
            }
            renderPointCloud(viewer, obsRoad.second, "road", Color(0, 1, 0));

            // next iterator
            ++streamIterator;
            if (streamIterator == stream.end()) {
                streamIterator = stream.begin();
            }
            
            oldMillis = currentMillis;
        }
        viewer->spinOnce();
    }
}