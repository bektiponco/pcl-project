#include <iostream>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/kdtree/kdtree.h>

#include <boost/filesystem.hpp>
#include <chrono>
#include <thread>

class EnginePCL
{
private:
    /* data */
public:
    EnginePCL(/* args */){};
    ~EnginePCL(){};

    struct Color{
        float r;
        float g;
        float b;

        Color(
            float setR,
            float setG,
            float setB)
            : r(setR)
            , g(setG)
            , b(setB)
        {}

        static Color random(){
            float red = rand() % 256;   // Merah
            float green = rand() % 256; // Hijau
            float blue = rand() % 256;  // Biru
            return Color(float(red)/256, float(green)/256, float(blue)/256);
        }
    };

    std::vector<boost::filesystem::path> streamPcd(std::string dataPath);
    pcl::PointCloud<pcl::PointXYZ>::Ptr loadPCD(std::string file);

    // ENGINE
    void renderPointCloud(
        pcl::visualization::PCLVisualizer::Ptr&    viewer,
        const pcl::PointCloud<pcl::PointXYZ>::Ptr& cloud,
        std::string                                name,
        Color                                      color);

    // PROCESS
    void start(std::vector<boost::filesystem::path>  path);
};
