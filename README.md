Before running, you must install:
1. PCL
2. VTK

Install PCL in linux,
1. sudo apt install libpcl-dev

Install VTK in linux,
1. Download VTK from the website, https://vtk.org/download/
2. tar -xzvf VTK-9.3.0.tar.gz
3. cd VTK-9.3.0/
4. mkdir build && cd build
5. cmake ..
6. make -j4
7. sudo make install

For build and running
1. ./running.sh