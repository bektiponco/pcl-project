#include "src/EnginePCL.h"

int main ()
{
  EnginePCL engine;
  std::vector<boost::filesystem::path> stream = engine.streamPcd("data");
  engine.start(stream);
  return (0);
}